package com.spring.smartpos187.service;

import com.spring.smartpos187.model.UserModel;

public interface UserService {

	public UserModel searchUsernamePassword(String username, String password);
}
