package com.spring.smartpos187.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController extends UserController{
	
	@RequestMapping(value="/login")
	public ModelAndView login(
			@RequestParam(value="error", required=false) String error,
			@RequestParam(value="logout", required=false) String logout
			) {
		ModelAndView modelAndView = new ModelAndView();
		if (error!=null) {
			modelAndView.addObject("error", "invalid username and password");
		} else {

		}
		
		if (logout!=null) {
			modelAndView.addObject("msg", "Logout successfully");
		} else {

		}
		return modelAndView;
	}
	
	@RequestMapping(value="/index")
	public ModelAndView index(Model model) {
		//this.aksesLogin(model);
		
		model.addAttribute("username", this.userSearch().getUsername());
		model.addAttribute("namaRole", this.userSearch().getRoleModel().getNamaRole());
		model.addAttribute("kodeRole", this.userSearch().getRoleModel().getKodeRole());
		return new ModelAndView("/index");
	}
	
	public void aksesLogin(Model model) {
		model.addAttribute("username", this.userSearch().getUsername());
		model.addAttribute("namaRole", this.userSearch().getRoleModel().getNamaRole());
		System.out.println("ROLE"+this.userSearch().getRoleModel().getNamaRole());
	}

}
