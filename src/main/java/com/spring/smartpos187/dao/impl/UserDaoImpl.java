package com.spring.smartpos187.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.smartpos187.dao.UserDao;
import com.spring.smartpos187.model.UserModel;

@Repository
public class UserDaoImpl implements UserDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public UserModel searchUsernamePassword(String username, String password) {
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		
		UserModel userModel = new UserModel();
		String query = " from UserModel "
				     + " where username='"+username+"' "
				     + " and password='"+password+"' ";
		userModel =  (UserModel) session.createQuery(query).uniqueResult();
		return userModel;
	}
	
	

}
