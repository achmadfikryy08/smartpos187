package com.spring.smartpos187.dao;

import com.spring.smartpos187.model.UserModel;

public interface UserDao {

	public UserModel searchUsernamePassword(String username, String password);
}
